package com.example.moviebookingsystem.demo.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Seat {
    @Id
    private Integer seatId;
    private Integer seatNumber;
    private Integer screenId;

    // getters and setters
}