package com.example.moviebookingsystem.demo.entity;
import jakarta.persistence.Id;
import java.util.List;

public class Ticket {

    @Id
    private Integer ticketId;
    private Integer showId;
    private Integer movieId;
    private List<Integer> showSeatIds;
    // getters and setters
}