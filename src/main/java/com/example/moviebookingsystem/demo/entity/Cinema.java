package com.example.moviebookingsystem.demo.entity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cinema {
    @Id
    private Integer cinemaId;
    private String cinemaName;
    private Integer cityId;

    // getters and setters
}