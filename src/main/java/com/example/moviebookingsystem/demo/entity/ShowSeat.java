package com.example.moviebookingsystem.demo.entity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor

public class ShowSeat {
    @Id
    private Long id;
    private Integer seatId;
    private Integer showId;
    private String status;

    // getters and setters
}