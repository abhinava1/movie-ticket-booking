package com.example.moviebookingsystem.demo.entity;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Shows {
    @Id
    private Integer showId;
//    private String timing;
    private Integer screenId;
    private Integer movieId;
    private Integer cinemaId;
    private Integer cityId;

    // getters and setters
}