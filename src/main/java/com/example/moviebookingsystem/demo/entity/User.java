package com.example.moviebookingsystem.demo.entity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    private Integer userId;
    @Column(unique = true)
    private String email;
    private String firstName;
    private String lastName;
    private String password;
    private String role;

    // getters and setters
}