package com.example.moviebookingsystem.demo.entity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Movie {
    @Id
    private Integer movieId;
    private String movieName;
    private String genre;
    private String releaseDate;
    private String language;
    private Integer duration;

    // getters and setters
}