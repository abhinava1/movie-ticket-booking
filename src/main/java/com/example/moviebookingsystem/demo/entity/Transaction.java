package com.example.moviebookingsystem.demo.entity;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class Transaction {

    @Id
    private Integer transactionId;
    private Integer userId;
    private Integer bookingId;
    private String paymentStatus;
    private int  amount;

    // getters and setters
}