package com.example.moviebookingsystem.demo.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Booking {
    @Id
    private Integer bookingId;
    private Integer showId;
    private Integer userId;
    private long bookingTime;
    private String bookingStatus;

    // getters and setters
}