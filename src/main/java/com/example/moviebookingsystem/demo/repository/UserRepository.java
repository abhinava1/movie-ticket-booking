package com.example.moviebookingsystem.demo.repository;
import com.example.moviebookingsystem.demo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
public interface UserRepository extends JpaRepository<User, Integer> {}