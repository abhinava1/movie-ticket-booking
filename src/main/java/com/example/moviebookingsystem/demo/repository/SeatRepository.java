package com.example.moviebookingsystem.demo.repository;

import com.example.moviebookingsystem.demo.entity.Seat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SeatRepository extends JpaRepository<Seat, Integer> {}