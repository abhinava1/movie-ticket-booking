package com.example.moviebookingsystem.demo.repository;
import com.example.moviebookingsystem.demo.entity.Cinema;
import org.springframework.data.jpa.repository.JpaRepository;
public interface CinemaRepository extends JpaRepository<Cinema, Integer> {}