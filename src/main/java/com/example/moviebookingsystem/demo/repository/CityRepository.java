package com.example.moviebookingsystem.demo.repository;


import com.example.moviebookingsystem.demo.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepository extends JpaRepository<City, Integer> {}