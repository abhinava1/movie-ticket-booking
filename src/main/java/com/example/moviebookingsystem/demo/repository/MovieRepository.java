package com.example.moviebookingsystem.demo.repository;

import com.example.moviebookingsystem.demo.entity.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie, Integer> {}