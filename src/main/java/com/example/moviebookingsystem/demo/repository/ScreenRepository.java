package com.example.moviebookingsystem.demo.repository;

import com.example.moviebookingsystem.demo.entity.Screen;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScreenRepository extends JpaRepository<Screen, Integer> {}