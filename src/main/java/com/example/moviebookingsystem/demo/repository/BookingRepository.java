package com.example.moviebookingsystem.demo.repository;
import com.example.moviebookingsystem.demo.entity.Booking;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingRepository extends JpaRepository<Booking, Integer> {}
