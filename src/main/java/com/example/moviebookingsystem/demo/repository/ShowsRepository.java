package com.example.moviebookingsystem.demo.repository;
import com.example.moviebookingsystem.demo.entity.Shows;
import org.springframework.data.jpa.repository.JpaRepository;
public interface ShowsRepository extends JpaRepository<Shows, Integer> {}