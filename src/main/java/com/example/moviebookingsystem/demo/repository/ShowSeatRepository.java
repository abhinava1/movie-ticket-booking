package com.example.moviebookingsystem.demo.repository;

import com.example.moviebookingsystem.demo.entity.ShowSeat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShowSeatRepository extends JpaRepository<ShowSeat, Long> {}