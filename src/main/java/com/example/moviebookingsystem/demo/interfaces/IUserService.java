package com.example.moviebookingsystem.demo.interfaces;

import com.example.moviebookingsystem.demo.entity.User;

public interface IUserService {  // already done
    void addUser(User user);
    String getRole(Integer userId);
}
