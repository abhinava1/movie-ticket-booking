package com.example.moviebookingsystem.demo.interfaces;
import com.example.moviebookingsystem.demo.entity.Ticket;
import com.example.moviebookingsystem.demo.entity.Transaction;

import java.util.List;
public interface IBookingService {  // abhinav and // omi
    boolean bookTicket(int userId, List<Integer> showSeatIds);
    void addTicket(Ticket ticket);
    void addTransaction(Transaction transaction);
    boolean doPayment();

}
