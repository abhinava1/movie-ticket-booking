package com.example.moviebookingsystem.demo.interfaces;
import com.example.moviebookingsystem.demo.entity.*;
public interface IAdminService{
    void addMovie(Movie movie);  //aditi
    void addCity(City city);  //aditi
    void addCinema(Cinema cinema);  //aditi
    void addScreen(Screen screen);  //aditi
    void addSeat(Seat seat);  //aditi
    void addShow(Shows shows); //aditi
    void addShowSeat(ShowSeat showSeat);  //aditi

}
