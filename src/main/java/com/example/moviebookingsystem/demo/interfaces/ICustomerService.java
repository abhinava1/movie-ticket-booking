package com.example.moviebookingsystem.demo.interfaces;
import com.example.moviebookingsystem.demo.entity.Cinema;
import com.example.moviebookingsystem.demo.entity.Movie;
import java.util.List;
public interface ICustomerService {  //Soumyadeep

    List<Movie> getMoviesByLanguage(String language);
    List<Movie> getMoviesByLocation(String location);
    List<Movie> getMoviesByLocationAndLanguage(String location, String language);

    List<Cinema> getCinemasByLocation(String location);
    List<Cinema> getCinemasByMovie(Integer movieId);
    List<Cinema> getCinemasByMovieAndLocation(Integer movieId, String location);

}
